import firebase from 'firebase'

const config = {
  apiKey: "AIzaSyDbJ8S65uU44tJMeLMg16pikGoiMNqG9Xk",
  authDomain: "test-fd571.firebaseapp.com",
  databaseURL: "https://test-fd571.firebaseio.com",
  projectId: "test-fd571",
  storageBucket: "test-fd571.appspot.com",
  messagingSenderId: "856656083149"
}

firebase.initializeApp(config)

export const ref = firebase.database().ref()
export const firebaseAuth = firebase.auth